# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

interests = [
    {
        :screen_name => '@Gerrard8FanPage',
        :hashtags => ['LFC'],
        :user_mentions => ['MR11ok', 'DaveOCKOP']
    },
    {
        :screen_name => '@EdDzeko',
        :hashtags => ['BeTheDifference', 'DZEKO', 'DANIELEDEROSSI'],
        :user_mentions => ['NFSBiH', 'ASRomaEN']
    },
    {
        :screen_name => '@DjokerNole',
        :hashtags => ['BNPPM', 'MTVEMA'],
        :user_mentions => ['ATPWorldTour']
    }
]

interests.each do |interes|
  l_interest = Interest.new
  l_interest.hashtags = []
  l_interest.user_mentions = []
  l_interest.screen_name = interes[:screen_name]
  interes[:hashtags].each do |hashtag|
    l_interest.hashtags << hashtag
  end
  interes[:user_mentions].each do |user_mention|
    l_interest.user_mentions << user_mention
  end
  #puts l_interest.hashtags
  l_interest.save
end
puts 'Seeding completed'