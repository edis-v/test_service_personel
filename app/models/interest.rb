class Interest
  include Mongoid::Document
  field :screen_name, type: String
  field :hashtags, type: Array
  field :user_mentions, type: Array

  validates :screen_name, presence: true

end
