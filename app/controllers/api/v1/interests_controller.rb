class Api::V1::InterestsController < ApplicationController
  before_action :set_interest, except: [:index, :create]

  def index
    @interests = Interest.all
    render json: @interests, status: :ok
  end

  def show
    if @interest
    render json: @interest, status: :ok
    else
      render nothing: true, status: :not_found
    end
  end

  def update
    @interest.screen_name = params[:interest][:screen_name] if params[:interest][:screen_name].present?
    @interest.hashtags = make_array(params[:interest][:hashtags], '#') if params[:interest][:hashtags]
    @interest.user_mentions = make_array(params[:interest][:user_mentions], '@') if params[:interest][:user_mentions]
    if @interest.save
      render json: @interest, status: :ok
    else
      render json: @interest.errors, status: :bad_request
    end
  end

  def create
    #@interest = Interest.new(interest_params)
    @interest = Interest.new
    @interest.screen_name = params[:interest][:screen_name]
    @interest.hashtags = params[:interest][:hashtags] ? make_array(params[:interest][:hashtags], '#') : []
    @interest.user_mentions = params[:interest][:user_mentions] ? make_array(params[:interest][:user_mentions], '@') : []
    if @interest.save
      render json: @interest, status: :created
    else
      render json: @interest.errors, status: :bad_request
    end
  end

  def destroy
    if @interest.destroy
      render nothing: true,  status: :no_content
    else
      render json: @interest.errors, status: :not_found
    end
  end


  private

  # method which accepts two params: string and sign and then
  # creates array from given string
  def make_array(s, sign)
    array1 = s.split("#{sign}")
    array1.shift if array1.length > 1
    array1
  end

  def set_interest
    @interest = Interest.find(params[:id])
  end

  def interest_params
    params.require(:interest).permit(:screen_name, :hashtags, :user_mentions)
  end

end
