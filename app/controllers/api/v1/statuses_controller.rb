class Api::V1::StatusesController < ApplicationController

  require 'net/http'

  def pull_statuses
    interest = Interest.find(params[:id])
    url = URI.parse("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=#{params[:screen_name]}&count=5")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Get.new(url.request_uri)
    request['Authorization'] = Rails.application.secrets.request_authorization
    request['Accept-Encoding'] = 'json'
    request['Host'] = 'api.twitter.com'
    request['User-Agent'] = Rails.application.secrets.request_user_agent
    response = http.request(request)
    puts "First response: #{response.body}"
    response_array = []

    JSON.parse(response.body).each do |r|

      if interest.user_mentions.empty? && interest.hashtags.empty?
        response_array << r
      else
        tmp = false

        if !interest.user_mentions.empty?
          r['entities']['user_mentions'].each do |response_user_mention|
            interest.user_mentions.each do |interest_user_mention|
              if response_user_mention['screen_name'] == interest_user_mention
                response_array << r
                tmp = true
              end
            end
          end
          next if tmp
        end

        if !interest.hashtags.empty?
          r['entities']['hashtags'].each do |response_hashtag|
            interest.hashtags.each do |interest_hashtag|
              if response_hashtag['text'] == interest_hashtag
                response_array << r
                tmp = true
              end
            end
          end
          next if tmp
        end
      end
    end

    render json: response_array, status: :ok

  end

end
